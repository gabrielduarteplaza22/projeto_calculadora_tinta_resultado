<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaintCalculatorController extends Controller
{
    public function index()
    {
        return view('tinta_calculadora.calculoTinta');
    }

    public function calcular(Request $request)
    {
        $totalArea = 0;
        $totalAreaPortasEJanelas = 0;
        $paredes = $request->paredes;

        $contParedes = 1;
        foreach ($paredes as $parede) {

            $largura = $parede['largura'];
            $altura = $parede['altura'];
            $janela = $parede['janela'];
            $porta = $parede['porta'];

            if (isset($largura)) {
                // Verificação da área mínima e máxima
                $area = $largura * $altura;
                if ($area < 1 || $area > 50) {
                    return redirect()->back()->withInput()->with(['error' => 'A parede ' . $contParedes . ' deve ter uma área entre 1 e 50 metros quadrados.']);
                }

                // Verificação da área ocupada pelas portas e janelas
                $areaPorta = $porta * 0.8 * 1.9; // área de uma porta padrão (0.8m x 1.9m)
                $areaJanela = $janela * 2 * 1.2; // área de uma janela padrão (2m x 1.2m)
                $totalArea += $area;
                $totalAreaPortasEJanelas += $areaPorta + $areaJanela;
                // Verificação da altura de paredes com porta
                $alturaPorta = 1.9; // Altura padrão da porta
                if ($porta > 0) {

                    $alturaMinimaParede = $alturaPorta + 0.3; // Altura mínima da parede com porta
                    if ($altura < $alturaMinimaParede) {
                        return redirect()->back()->with('error', 'A altura da parede ' . $contParedes . ' com porta deve ser no mínimo 30 centímetros maior que a altura da porta.');
                    }
                }

                $contParedes++;
            }
        }

        // Verificação da área total
        if ($totalArea < $totalAreaPortasEJanelas * 2) {
            return redirect()->back()->with('error', 'O total de área das portas e janelas não pode ser maior que 50% da área da parede.');
        }

        $totalArea -= $totalAreaPortasEJanelas;
        // Cálculo da quantidade de tinta necessária em litros

        // Cálculo das latas de tinta necessárias
        $totalLitros = ceil($totalArea / 5);
        $totalLitrosTotal = ceil($totalArea / 5);
        // Calcular o número de latas a comprar para cada tipo de lata
        $latasComprar = [
            '0.5' => 0,
            '2.5' => 0,
            '3.6' => 0,
            '18' => 0,
        ];

        while ($totalLitros > 0) {
            if ($totalLitros >= 18) {
                $latasComprar['18']++;
                $totalLitros -= 18;
            } elseif ($totalLitros >= 3.6) {
                $latasComprar['3.6']++;
                $totalLitros -= 3.6;
            } elseif ($totalLitros >= 2.5) {
                $latasComprar['2.5']++;
                $totalLitros -= 2.5;
            } else {
                $latasComprar['0.5']++;
                $totalLitros -= 0.5;
            }
        }
        
        // Retornar os resultados para a view
        $data = [
            'totalLitrosTotal' => $totalLitrosTotal,
            'latasComprar' => $latasComprar,
            'totalArea' => $totalArea,
        ];

        return view('tinta_calculadora.resultado', $data);
    }
}
