<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Tinta</title>
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            background-color: #f8f8f8;
            font-family: Arial, sans-serif;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }

        h1 {
            text-align: center;
            margin-bottom: 30px;
        }

        form {
            display: grid;
            gap: 20px;
        }

        .wall-container {
            border: 1px solid #ccc;
            padding: 20px;
            border-radius: 5px;
            background-color: #f8f8f8;
        }

        .wall-container h2 {
            font-size: 18px;
            margin-bottom: 10px;
        }

        .form-group {
            display: grid;
            gap: 5px;
        }

        label {
            font-weight: bold;
        }

        input[type="number"] {
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        button[type="submit"] {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
            font-weight: bold;
            transition: background-color 0.3s ease;
        }

        button[type="submit"]:hover {
            background-color: #0069d9;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Calculadora de Tinta</h1>
        @if(session('error'))
        <div style="color: red;">{{ session('error') }}</div>
        @endif
        <form method="post" action="/calcular">
            @csrf
            @for($i = 1; $i <= 4; $i++)
            <div class="wall-container">
                <h2>Parede {{$i}}</h2>
                <div class="form-group">
                    <label for="tamanho{{$i}}">Altura da Parede (m²):</label>
                    <input type="number" name="paredes[{{$i}}][altura]" id="altura{{$i}}" step="0.01" required >
                </div>
                <div class="form-group">
                    <label for="largura{{$i}}">Largura da Parede (m²):</label>
                    <input type="number" name="paredes[{{$i}}][largura]" id="largura{{$i}}" step="0.01" required>
                </div>
                <div class="form-group">
                    <label for="janela{{$i}}">Quantidade de Janelas:</label>
                    <input type="number" name="paredes[{{$i}}][janela]" id="janela{{$i}}" step="1" required>
                </div>
                <div class="form-group">
                    <label for="porta{{$i}}">Quantidade de Portas:</label>
                    <input type="number" name="paredes[{{$i}}][porta]" id="porta{{$i}}" step="1" required>
                </div>
            </div>
            @endfor
            <button type="submit">Calcular</button>
        </form>
    </div>
</body>
</html>